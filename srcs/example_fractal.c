#include "fractal.h"

#define MLX_BPP			32
#define MLX_ENDIAN		0

char		*get_img_buffer(void *p_img)
{
	static int		bpp = MLX_BPP;
	static int		size_line = WINDOW_WIDTH;
	static int		endian = MLX_ENDIAN;

	return (mlx_get_data_addr(p_img, &bpp, &size_line, &endian));
}

t_complex	init_entry_point(void)
{
	t_complex	point;
	float		coord_height;
	float		window_width = WINDOW_WIDTH;

	point.r = -1.0f * COORD_WIDTH / 2.0f;
	coord_height = COORD_WIDTH * WINDOW_HEIGHT / window_width;
	point.i = -1.0f * coord_height / 2.0f;
	return (point);
}

float		init_delta(void)
{
	return (COORD_WIDTH / WINDOW_WIDTH);
}

void		example_fractal(void)
{
	int fd;
	int *host_buf;
	char *source_str;
	cl_int ret;
	cl_platform_id platform;
	cl_device_id device;
	cl_context context;
	cl_command_queue command_queue;
	cl_program program;
	cl_kernel kernel;
	cl_mem dev_mem_obj;
	size_t global_work_size = WINDOW_WIDTH * WINDOW_HEIGHT;
	size_t local_work_size = 1;
	t_complex entry_point;
	float delta;
	int window_width = WINDOW_WIDTH;

	void *p_mlx = mlx_init();
	void *p_win = mlx_new_window(p_mlx, WINDOW_WIDTH, WINDOW_HEIGHT, "fractal");
	void *p_img = mlx_new_image(p_mlx, WINDOW_WIDTH, WINDOW_HEIGHT);

	host_buf = (int *)get_img_buffer(p_img);

	entry_point = init_entry_point();
	delta = init_delta();

	/* 블로그에서는 아래 코드로 */
	// host_buf = (int *)malloc(sizeof(int) * global_work_size);

	/* 병렬 연산을 수행할 디바이스 ID 쿼리 */
	clGetPlatformIDs(1, &platform, NULL);
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);

	/* context 와 커맨드 큐 구조체 생성 */
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &ret);
	command_queue = clCreateCommandQueue(context, device, 0, &ret);

	/*
	** 아래는 .cl 파일의 소스를 문자열로 복사하는 코드입니다.
	** get_file_content 와 같은 기능을 하는 함수는 어렵지 않게 만드실 수 있습니다.
	*/
	fd = open("kernels/fractal.cl", O_RDONLY);
	source_str = get_file_content(fd);
	close(fd);

	/* 프로그램 구조체 생성 */
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, NULL, &ret);
	clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	free(source_str);

	/* 커널 구조체 생성 */
	kernel = clCreateKernel(program, "fractal", &ret);

	/* 메모리 오브젝트 생성 */
	dev_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY,\
		sizeof(*host_buf) * global_work_size, NULL, &ret);

	/* 커널 매개변수 설정 */
	clSetKernelArg(kernel, 0, sizeof(dev_mem_obj), &dev_mem_obj);
	clSetKernelArg(kernel, 1, sizeof(entry_point), &entry_point);
	clSetKernelArg(kernel, 2, sizeof(delta), &delta);
	clSetKernelArg(kernel, 3, sizeof(window_width), &window_width);

	/* 커맨드 큐에 커널과 메모리 읽는 커맨드 추가 */
	clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,\
		&global_work_size, &local_work_size, 0, NULL, NULL);
	clEnqueueReadBuffer(command_queue, dev_mem_obj, CL_TRUE, 0,\
		sizeof(*host_buf) * global_work_size, host_buf, 0, NULL, NULL);

	/* 커맨드 큐에 추가된 커맨드 실행 */
	clFlush(command_queue);
	clFinish(command_queue);

	/* 필요한 구조체 메모리 해제 */
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(dev_mem_obj);
	clReleaseCommandQueue(command_queue);
	clReleaseContext(context);
	printf("done\n");

	mlx_put_image_to_window(p_mlx, p_win, p_img, 0, 0);
	mlx_loop(p_mlx);
}