#include "opencl_tutorial.h"

void		device_info(void)
{
	char				*value;
	size_t				value_size;
	cl_uint				plaform_count;
	cl_uint				device_count;
	cl_uint				compute_units;
	cl_platform_id		*platforms;
	cl_device_id		*devices;

	clGetPlatformIDs(0, NULL, &plaform_count);
	platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * plaform_count);
	clGetPlatformIDs(plaform_count, platforms, NULL);
	
	for (int i=0; i < plaform_count; i++)
	{
		/*
		** Platform info 
		*/
		printf(KGRN "Plaform %d\n" KNRM, i);

		// name
		clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 0, NULL, &value_size);
		value = (char *)malloc(sizeof(char) * value_size);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, value_size, value, NULL);
		printf(KYEL "Name %s\n", value);
		free(value);

		// profile
		clGetPlatformInfo(platforms[i], CL_PLATFORM_PROFILE, 0, NULL, &value_size);
		value = (char *)malloc(sizeof(char) * value_size);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_PROFILE, value_size, value, NULL);
		printf(KYEL "Profile %s\n", value);
		free(value);

		// vendor
		clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, 0, NULL, &value_size);
		value = (char *)malloc(sizeof(char) * value_size);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, value_size, value, NULL);
		printf(KYEL "Vendor %s\n", value);
		free(value);

		// version
		clGetPlatformInfo(platforms[i], CL_PLATFORM_VERSION, 0, NULL, &value_size);
		value = (char *)malloc(sizeof(char) * value_size);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_VERSION, value_size, value, NULL);
		printf(KYEL "Version %s\n", value);
		free(value);

		// extensions
		clGetPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, 0, NULL, &value_size);
		value = (char *)malloc(sizeof(char) * value_size);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, value_size, value, NULL);
		printf(KYEL "Extensions %s\n", value);
		free(value);

		clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &device_count);
		devices = (cl_device_id *)malloc(sizeof(cl_device_id) * device_count);
		clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, device_count, devices, NULL);
		for (int j=0; j < device_count; j++)
		{
			/*
			** Device info
			*/
			printf(KRED "\nDevice %d\n" KNRM, j);

			// name
			clGetDeviceInfo(devices[j], CL_DEVICE_NAME, 0, NULL, &value_size);
			value = (char *)malloc(sizeof(char) * value_size);
			clGetDeviceInfo(devices[j], CL_DEVICE_NAME, value_size, value, NULL);
			printf(KYEL "Name %s\n" KNRM, value);

			// device version
			clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, 0, NULL, &value_size);
			value = (char *)malloc(sizeof(char) * value_size);
			clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, value_size, value, NULL);
			printf(KYEL "Device version %s\n" KNRM, value);

			// driver version
			clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, 0, NULL, &value_size);
			value = (char *)malloc(sizeof(char) * value_size);
			clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, value_size, value, NULL);
			printf(KYEL "Driver version %s\n" KNRM, value);

			// opencl c version
			clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &value_size);
			value = (char *)malloc(sizeof(char) * value_size);
			clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, value_size, value, NULL);
			printf(KYEL "OpenCL c version %s\n" KNRM, value);

			// compute units
			clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS,\
				sizeof(compute_units), &compute_units, NULL);
			printf(KYEL "Compute units %d\n" KNRM, compute_units);
		}
		free(devices);
	}
	free(platforms);
}