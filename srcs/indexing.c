#include "opencl_tutorial.h"

#define NUM_ELEMENTS	256

void		indexing(void)
{
	int					fd;
	cl_int				ret;
	cl_platform_id		platform;
	cl_device_id		device;
	cl_uint				num_platforms;
	cl_uint				num_devices;
	cl_context			context;
	cl_program			program;
	cl_kernel			kernel;
	cl_command_queue	command_queue;
	int					*host_buf;
	cl_mem				dev_mem_obj;
	size_t				global_work_size = NUM_ELEMENTS;
	size_t				local_work_size = 128;
	char				*source_str;

	/*
	** prerequisites
	*/
	clGetPlatformIDs(1, &platform, &num_platforms);
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, &num_devices);
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &ret);
	command_queue = clCreateCommandQueue(context, device, 0, &ret);

	/*
	** make memory
	*/
	host_buf = (int *)ft_memalloc(sizeof(int) * NUM_ELEMENTS);
	for (size_t i=0; i < NUM_ELEMENTS; i++)
		host_buf[i] = -1;
	dev_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY,\
		sizeof(*host_buf) * NUM_ELEMENTS, NULL, &ret);

	/*
	** make kernel
	*/
	fd = open("kernels/setidx.cl", O_RDONLY);
	source_str = get_file_content(fd);
	program = clCreateProgramWithSource(context, 1,\
		(const char **)&source_str, NULL, &ret);
	ret = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	if (ret != CL_SUCCESS)
		printf(KRED "Build error\n" KNRM);
	kernel = clCreateKernel(program, "setidx", &ret);
	clSetKernelArg(kernel, 0, sizeof(dev_mem_obj), &dev_mem_obj);

	/*
	** execute kernel
	*/
	clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,\
		&global_work_size, &local_work_size, 0, NULL, NULL);
	clEnqueueReadBuffer(command_queue, dev_mem_obj, CL_TRUE, 0,\
		sizeof(*host_buf) * NUM_ELEMENTS, host_buf, 0, NULL, NULL);
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);

	if (ret != CL_SUCCESS)
		printf(KRED "Fail executing Kernel\n" KNRM);
	else
	{
		for (size_t i=0; i < NUM_ELEMENTS; i++)
			printf(KGRN "%d " KNRM, host_buf[i]);
	}

	close(fd);
	ft_memdel((void **)&host_buf);
	ft_memdel((void **)&source_str);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(dev_mem_obj);
	clReleaseCommandQueue(command_queue);
	clReleaseContext(context);
}