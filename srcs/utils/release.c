#include "opencl_tutorial.h"

void		release(cl_kernel *kernel, cl_program *program, cl_mem *mem,\
		cl_command_queue *command_queue, cl_context *context)
{
	clReleaseKernel(*kernel);
	clReleaseProgram(*program);
	if (mem != NULL)
		clReleaseMemObject(*mem);
	clReleaseCommandQueue(*command_queue);
	clReleaseContext(*context);
}
