#include "opencl_tutorial.h"

int			make_kernel(const char *source_filename, cl_program *program, cl_kernel *kernel,\
		cl_context *context, cl_device_id *device, const char *kernel_name)
{
	int			fd;
	cl_int		ret;
	char		*source_str;

	fd = open(source_filename, O_RDONLY);
	if (fd == -1)
	{
		printf(KRED "File not open\n" KNRM);
		return (-1);
	}
	source_str = get_file_content(fd);
	*program = clCreateProgramWithSource(*context, 1, (const char **)&source_str, NULL, &ret);
	if (ret != CL_SUCCESS)
	{
		printf(KRED "Error create Program\n" KNRM);
		return (-1);
	}
	ret = clBuildProgram(*program, 1, device, NULL, NULL, NULL);
	if (ret != CL_SUCCESS)
	{
		printf(KRED "Error build Program\n" KNRM);
		return (-1);
	}
	*kernel = clCreateKernel(*program, kernel_name, &ret);
	if (ret != CL_SUCCESS)
	{
		printf(KRED "Error create Kernel\n" KNRM);
		return (-1);
	}
	ft_memdel((void **)&source_str);
	close(fd);
	printf(KGRN "Success make kernel\n" KNRM);
	return (1);
}
