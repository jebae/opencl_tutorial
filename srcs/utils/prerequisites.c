#include "opencl_tutorial.h"

int			prerequisites(cl_device_id *device, cl_context *context, cl_command_queue *command_queue)
{
	cl_int				ret;
	cl_platform_id		platform;
	cl_uint				num_platforms;
	cl_uint				num_devices;

	clGetPlatformIDs(1, &platform, &num_platforms);
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, device, &num_devices);
	*context = clCreateContext(NULL, 1, device, NULL, NULL, &ret);
	if (ret != CL_SUCCESS)
	{
		printf(KRED "Error create Context\n" KNRM);
		return (-1);
	}
	*command_queue = clCreateCommandQueue(*context, *device, 0, &ret);
	if (ret != CL_SUCCESS)
	{
		printf(KRED "Error create Command Queue\n" KNRM);
		return (-1);
	}
	printf(KGRN "Success prerequisites\n" KNRM);
	return (1);
}
