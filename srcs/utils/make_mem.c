#include "opencl_tutorial.h"

int			make_mem(void **host_buf, cl_mem *dev_mem_obj,\
		cl_context *context, size_t size, cl_mem_flags flags)
{
	cl_int		ret;

	*host_buf = ft_memalloc(size);
	*dev_mem_obj = clCreateBuffer(*context, flags, size, NULL, &ret);
	if (ret != CL_SUCCESS)
	{
		printf(KRED "Error create Buffer\n" KNRM);
		return (-1);
	}
	printf(KGRN "Success make mem\n" KNRM);
	return (1);
}
