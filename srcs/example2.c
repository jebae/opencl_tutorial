#include "opencl_tutorial.h"
#define NUM_ELEMENTS_X 16
#define NUM_ELEMENTS_Y 16
#define NUM_ELEMENTS (NUM_ELEMENTS_X * NUM_ELEMENTS_Y)

void		example2(void)
{
	int fd;
	int *host_buf;
	char *source_str;
	cl_int ret;
	cl_platform_id platform;
	cl_device_id device;
	cl_context context;
	cl_command_queue command_queue;
	cl_program program;
	cl_kernel kernel;
	cl_mem dev_mem_obj;
	size_t global_work_size[2] = {NUM_ELEMENTS_X, NUM_ELEMENTS_Y};
	size_t local_work_size[2] = {4, 4};

	host_buf = (int *)malloc(sizeof(int) * NUM_ELEMENTS);

	/* 병렬 연산을 수행할 디바이스 ID 쿼리 */
	clGetPlatformIDs(1, &platform, NULL);
	clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);

	/* context 와 커맨드 큐 구조체 생성 */
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &ret);
	command_queue = clCreateCommandQueue(context, device, 0, &ret);

	/*
	** 아래는 .cl 파일의 소스를 문자열로 복사하는 코드입니다.
	** get_file_content 와 같은 기능을 하는 함수는 어렵지 않게 만드실 수 있습니다.
	*/
	fd = open("kernels/set_group_index.cl", O_RDONLY);
	source_str = get_file_content(fd);
	close(fd);

	/* 프로그램 구조체 생성 */
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, NULL, &ret);
	clBuildProgram(program, 1, &device, NULL, NULL, NULL);
	clk_get_program_build_info(program, device);

	/* 커널 구조체 생성 */
	kernel = clCreateKernel(program, "set_group_index", &ret);

	/* 메모리 오브젝트 생성 */
	dev_mem_obj = clCreateBuffer(context, CL_MEM_WRITE_ONLY,\
		sizeof(*host_buf) * NUM_ELEMENTS, NULL, &ret);

	/* 커널 매개변수 설정 */
	clSetKernelArg(kernel, 0, sizeof(dev_mem_obj), &dev_mem_obj);

	/* 커맨드 큐에 커널과 메모리 읽는 커맨드 추가 */
	clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,\
		global_work_size, local_work_size, 0, NULL, NULL);
	clEnqueueReadBuffer(command_queue, dev_mem_obj, CL_TRUE, 0,\
		sizeof(*host_buf) * NUM_ELEMENTS, host_buf, 0, NULL, NULL);

	/* 커맨드 큐에 추가된 커맨드 실행 */
	clFlush(command_queue);
	clFinish(command_queue);

	/* 필요한 구조체 메모리 해제 */
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(dev_mem_obj);
	clReleaseCommandQueue(command_queue);
	clReleaseContext(context);

	for (int i=0; i < NUM_ELEMENTS_Y; i++)
	{
		for (int j=0; j < NUM_ELEMENTS_X; j++)
			printf("%3d ", host_buf[NUM_ELEMENTS_X * i + j]);
		printf("\n");
	}
}