#include "opencl_tutorial.h"
#define NUM_ELEMENTS_X		16
#define NUM_ELEMENTS_Y		16
#define NUM_ELEMENTS		(NUM_ELEMENTS_X * NUM_ELEMENTS_Y)

void		disp_buf(int *buf)
{
	for (int i=0; i < NUM_ELEMENTS_Y; i++)
	{
		for (int j=0; j < NUM_ELEMENTS_X; j++)
			printf("%3d ", buf[NUM_ELEMENTS_X * i + j]);
		printf("\n");
	}
}

void		multidim(void)
{
	cl_device_id		device;
	cl_context			context;
	cl_command_queue	command_queue;
	int					*host_buf;
	cl_mem				dev_mem_obj;
	cl_program			program;
	cl_kernel			kernel;
	size_t				global_work_size[2] = {NUM_ELEMENTS_X, NUM_ELEMENTS_Y};
	size_t				local_work_size[2] = {4, 4};

	if (prerequisites(&device, &context, &command_queue) == -1)
		return ;
	if (make_mem((void **)&host_buf, &dev_mem_obj,\
				&context, sizeof(int) * NUM_ELEMENTS, CL_MEM_WRITE_ONLY) == -1)
		return ;
	if (make_kernel("kernels/set_group_index.cl", &program, &kernel,\
		&context, &device, "set_group_index") == -1)
		return ;
	clSetKernelArg(kernel, 0, sizeof(cl_mem), &dev_mem_obj);
	clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL,\
			global_work_size, local_work_size, 0, NULL, NULL);
	clEnqueueReadBuffer(command_queue, dev_mem_obj, CL_TRUE, 0,\
			sizeof(*host_buf) * NUM_ELEMENTS, host_buf, 0, NULL, NULL);
	if (clFlush(command_queue) != CL_SUCCESS)
	{
		printf(KRED "Error flush Queue\n" KNRM);
		return ;
	}
	if (clFinish(command_queue) != CL_SUCCESS)
	{
		printf(KRED "Error finish Queue\n" KNRM);
		return ;
	}
	disp_buf(host_buf);
	ft_memdel((void **)&host_buf);
	release(&kernel, &program, &dev_mem_obj, &command_queue, &context);
}
