#include "opencl_tutorial.h"
#define NUM_ELEMENTS	1024

void		clock(void)
{
	cl_device_id		device;
	cl_context			context;
	cl_command_queue	command_queue;
	cl_program			program;
	cl_kernel			kernel;
	cl_mem				dev_mem_obj;
	unsigned int		*host_buf;
	size_t				global_work_size = NUM_ELEMENTS;
	size_t				local_work_size = 1;
	cl_int				ret;

	if (prerequisites(&device, &context, &command_queue) == -1)
		return ;
	if (make_mem((void **)&host_buf, &dev_mem_obj, &context,\
				sizeof(unsigned int) * NUM_ELEMENTS, CL_MEM_WRITE_ONLY) == -1)
		return ;
	if (make_kernel("kernels/clock.cl", &program, &kernel,\
		&context, &device, "settoclock") == -1)
		return ;
	clSetKernelArg(kernel, 0, sizeof(cl_mem), &dev_mem_obj);
	clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,\
			&global_work_size, &local_work_size, 0, NULL, NULL);
	clEnqueueReadBuffer(command_queue, dev_mem_obj, CL_TRUE, 0,\
			sizeof(*host_buf) * NUM_ELEMENTS, host_buf, 0, NULL, NULL);
	clFlush(command_queue);
	ret = clFinish(command_queue);
	if (clFlush(command_queue) != CL_SUCCESS)
	{
		printf(KRED "Error flush Queue\n" KNRM);
		return ;
	}
	if (clFinish(command_queue) != CL_SUCCESS)
	{
		printf(KRED "Error finish Queue\n" KNRM);
		return ;
	}
	for (int i=0; i < NUM_ELEMENTS; i++)
		printf("%u\n", host_buf[i]);
	release(&kernel, &program, NULL, &command_queue, &context);
}
