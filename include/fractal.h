#ifndef FRACTAL_H
# define FRACTAL_H

# include <OpenCL/cl.h>
# include <stdio.h>
# include <fcntl.h>
# include "libft.h"
# include "clkit.h"
# include <mlx.h>

# define COORD_WIDTH		5.0f
# define WINDOW_WIDTH		1000
# define WINDOW_HEIGHT		800

typedef struct		s_complex
{
	float	r;
	float	i;
}					t_complex;

t_complex			init_entry_point(void);
float				init_delta(void);
void				example_fractal(void);

#endif