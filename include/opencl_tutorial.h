#ifndef OPENCL_TUTORIAL_H
# define OPENCL_TUTORIAL_H

# include <OpenCL/cl.h>
# include <stdio.h>
# include <fcntl.h>
# include "libft.h"

# define KRED		"\x1B[31m"
# define KGRN		"\x1B[32m"
# define KYEL		"\x1B[33m"
# define KNRM		"\x1B[0m"

void			device_info(void);

/*
** utils
*/
int				prerequisites(cl_device_id *device, cl_context *context,\
		cl_command_queue *command_queue);
int				make_mem(void **host_buf, cl_mem *dev_mem_obj,\
		cl_context *context, size_t size, cl_mem_flags flags);
int				make_kernel(const char *source_filename, cl_program *program, cl_kernel *kernel,\
		cl_context *context, cl_device_id *device, const char *kernel_name);
void			release(cl_kernel *kernel, cl_program *program, cl_mem *mem,\
		cl_command_queue *command_queue, cl_context *context);

/*
** tutorials
*/
void			heterogeneous(void);
void			indexing(void);
void			multidim(void);
void			clock(void);
void			example1(void);
void			example2(void);

#endif
