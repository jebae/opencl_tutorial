__kernel	void	set_minus_one(__global int *out)
{
	int		i = get_global_id(0);

	out[i] = -1;
}
