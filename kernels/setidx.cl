__kernel	void	get_unit_idx(__global int *out)
{
	int		idx = get_global_id(0);

	out[idx] = idx;
}
