kernel	void	set_group_index(global int *out)
{
	int		idx_x = get_group_id(0) * get_local_size(0) + get_local_id(0);
	int		idx_y = get_group_id(1) * get_local_size(1) + get_local_id(1);
	int		row_size = get_num_groups(0) * get_local_size(0);
	int		idx = idx_y * row_size + idx_x;
	int		result = get_local_size(0) * get_local_size(1) *\
			(get_num_groups(0) * get_group_id(1) + get_group_id(0)) +\
			(get_local_id(1) * get_local_size(0) + get_local_id(0));

	out[idx] = result;
}
