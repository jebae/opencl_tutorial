unsigned int	clock_time(void)
{
	unsigned int	clock_time;

	asm("mov.u32 %0, %%clock;" : "=r"(clock_time));
	return clock_time;
}

kernel void		settoclock(global unsigned int *out)
{
	int		idx = get_global_id(0);

	out[idx] = clock_time();
}
