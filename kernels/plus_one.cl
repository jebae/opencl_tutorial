kernel	void	plus_one(global int *in, global int *out)
{
	int		idx_x = get_global_id(0);
	int		idx_y = get_global_id(1);
	int		idx = idx_x + idx_y * get_global_size(0);

	out[idx] = in[idx] + 1;
}
