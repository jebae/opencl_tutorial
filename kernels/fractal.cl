#define MAX_ITERATION		100
#define WHITE				0xFFFFFF
#define BLACK				0x000000

typedef struct		s_complex
{
	float	r;
	float	i;
}					t_complex;

int					iteration(t_complex z, t_complex c);

int					iteration(t_complex z, t_complex c)
{
	int			i;
	float		aa;
	float		bb;
	float		two_ab;

	i = 0;
	while (i < MAX_ITERATION)
	{
		aa = z.r * z.r;
		bb = z.i * z.i;
		two_ab = 2 * z.r * z.i;
		if (aa + bb > 4)
			return (WHITE);
		z.r = aa - bb + c.r;
		z.i = two_ab + c.i;
		i++;
	}
	return (BLACK);
}

__kernel void		fractal(
	__global int *out,
	t_complex entry_point,
	float delta,
	int window_width
)
{
	int				idx = get_global_id(0);
	int				r = idx % window_width;
	int				i = idx / window_width;
	t_complex		point;

	point.r = entry_point.r + delta * r;
	point.i = entry_point.i + delta * i;
	out[idx] = iteration(point, point);
}